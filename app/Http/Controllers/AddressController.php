<?php

namespace App\Http\Controllers;

use App\Http\Requests\employee\CreateEmployeeAddressRequest;

use App\Models\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $addresses = Address::ofUser()->get();
        return view('address.index',compact('addresses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('address.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEmployeeAddressRequest $request)
    {

        Address::create([
            'user_id' => auth()->id(),
            'address' => $request->address,
            'landmark' => $request->landmark,
            'city' => $request->city,
            'district' => $request->district,
            'state' => $request->state,
            'postal_code' => $request->postal_code
        ]);
        return redirect(route('addresses.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function show(Address $address)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function edit(Address $address)
    {
        $this->authorize('update',$address);
        return view('address.edit',compact('address'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function update(CreateEmployeeAddressRequest $request, Address $address)
    {
        $this->authorize('update',$address);
       $address->update([
           'address' => $request->address,
           'landmark' => $request->landmark,
           'city' => $request->city,
           'district' => $request->district,
           'state' => $request->state,
           'postal_code' => $request->postal_code
       ]);
       return redirect(route('addresses.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function destroy(Address $address)
    {
        $this->authorize('forceDelete',$address);
        $address->delete();
        return redirect(route('addresses.index'));
    }
}
