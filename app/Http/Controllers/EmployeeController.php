<?php

namespace App\Http\Controllers;

use App\Http\Requests\employee\CreateEmployeeAddressRequest;
use App\Http\Requests\employee\CreateEmployeeBasicsRequest;
use App\Http\Requests\employee\CreateEmployeePostRequest;
use App\Http\Requests\employee\CreateEmployeeRequest;
use App\Models\Address;
use App\Models\Contact;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class EmployeeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Employee::isAdmin())
        {
            $employees = Employee::with('loginDetails')->paginate(10);
            return view('employee.index',compact('employees'));
        }
        else
        {
            abort(403,'Access Denied!');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_basic()
    {
        if(Employee::isAdmin())
        {
            return view('employee.create-basic');
        }
        else
        {
            abort(403,'Access Denied!');
        }

    }
    public function create_address()
    {
        if(Employee::isAdmin())
        {
            return view('employee.create-address');
        }
        else
        {
            abort(403,'Access Denied!');
        }
    }
    public function create_post()
    {
        if(Employee::isAdmin())
        {
            return view('employee.create-post');
        }
        else
        {
            abort(403,'Access Denied!');
        }

    }
    // public function create()
    // {
    //     return view('employee.create');
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_basic(CreateEmployeeBasicsRequest $request)
    {
        if(!(Employee::isAdmin()))
        {
            abort(403,'Access Denied!');
        }
        $whatsapp_status = 0;
        if($request->is_whatsapp_number != null)
        {
            $whatsapp_status = 1;
        }
        session([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'is_whatsapp_number' => $whatsapp_status
        ]);
        return redirect(route('employees.create-address'));
    }
    public function store_address(CreateEmployeeAddressRequest $request)
    {

        if(!(Employee::isAdmin()))
        {
            abort(403,'Access Denied!');
        }
        session([
            'address' => $request->address,
            'landmark' => $request->landmark,
            'city' => $request->city,
            'district' => $request->district,
            'state' => $request->state,
            'postal_code' => $request->postal_code
        ]);

        return redirect(route('employees.create-post'));
    }
    public function store(CreateEmployeePostRequest $request)
    {
        if(!(Employee::isAdmin()))
        {
            abort(403,'Access Denied!');
        }
        // $password = User::generatePassword();
        $user = User::create([
           'name' => session()->get('name'),
           'email' => session()->get('email'),
           'password' => Hash::make("password"),
           'role' => $request->role
        ]);
        Contact::create([
            'user_id' => $user->id,
            'phone' => session()->get('phone'),
            'is_whatsapp_number' => 1
        ]);
        Address::create([
            'user_id' => $user->id,
            'address' => session()->get('address'),
            'landmark' => session()->get('landmark'),
            'city' => session()->get('city'),
            'district' => session()->get('district'),
            'state' => session()->get('state'),
            'postal_code' => session()->get('postal_code')
        ]);
        // send the generated password to the user via mail
        Employee::create([
            'user_id' => $user->id,
            'salary' => $request->salary
        ]);
        return redirect(route('employees.index'));
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }
}
