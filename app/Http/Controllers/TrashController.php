<?php

namespace App\Http\Controllers;


use App\Http\Requests\trash\CreateTrashPickedRequest;
use App\Http\Requests\trash\CreateTrashRequest;
use App\Models\Address;
use App\Models\Employee;
use App\Models\Trash;
use Illuminate\Http\Request;

class TrashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->isAdmin())
        {
            $trashes = Trash::address()->get();
        }
        else
        {
            $trashes = Trash::address()->userTrashes()->get();
        }
        return view('trash.index',compact('trashes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $addresses = Address::ofUser()->get();
        return view('trash.create',compact('addresses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTrashRequest $request)
    {
        Trash::create([
            'address_id' => $request->address_id,
            'type' => $request->type,
            'status' => 'booked'
        ]);
        return redirect(route('trashes.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Trash  $trash
     * @return \Illuminate\Http\Response
     */
    public function show(Trash $trash)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Trash  $trash
     * @return \Illuminate\Http\Response
     */
    public function edit(Trash $trash)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Trash  $trash
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trash $trash)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Trash  $trash
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trash $trash)
    {
        //
    }
    public function adminPending()
    {
        $trashes = Trash::pending()->get();
        $employees = Employee::all();
        return view('trash.assign',compact('trashes','employees'));

    }
    public function adminAssign(Request $request,Trash $trash)
    {
        $trash->update([
            'assigned_to' => $request->assign,
            'status' => Trash::ASSIGNED

        ]);
        return redirect()->back();

    }
    public function assigned()
    {

        $trashes = Trash::assigned()->get();

        return view('trash.assigned',compact('trashes'));
    }
    public function picked(Request $request,Trash $trash)
    {
        $request->validate([
            'weight' => 'required'
        ]);
        $trash->update([
            'weight' => $request->weight,
            'status' => Trash::PICKED
        ]);
        return redirect()->back();
    }
    public function reject(Trash $trash)
    {
        $trash->update([
            'status' => Trash::REJECT
        ]);
        return redirect()->back();
    }
}
