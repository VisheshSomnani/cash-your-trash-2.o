<?php

namespace App\Http\Requests\trash;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateTrashRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address_id' => 'required', Rule::exists('address_id')->where(function ($query) {
                return $query->where('user_id', auth()->id());
            }),
            'type' => 'required'
        ];
    }
}
