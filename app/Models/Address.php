<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    //Relationship
    public function owner()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    //scopes
    public function scopeOfUser($query)
    {
        return $query->where('user_id','=',auth()->id());
    }

}
