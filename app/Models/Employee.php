<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function loginDetails()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function residence()
    {
        return Address::all()->where('user_id',$this->loginDetails()->get()->pluck('id')[0]);
    }
    public function contacts()
    {
        return Contact::all()->where('user_id',$this->loginDetails()->get()->pluck('id')[0]);
    }


    //helper functions
    public static function isAdmin()
    {
        return auth()->user()->role === User::ADMIN;
    }


}
