<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    const ADMIN = 'admin';
    const EMPLOYEE = 'employee';
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function isAdmin()
    {
        return $this->role === User::ADMIN;
    }

    public function isEmployee()
    {
        return $this->role === User::EMPLOYEE || $this->role === User::ADMIN;
    }
    public static function getEmployeeId($id)
    {
        $employee = Employee::where('user_id','=',$id)->firstOrFail()->id;
        return $employee;
    }

    //Relationships
    public function addresses()
    {
        return $this->hasMany(Address::class);
    }
    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }

    public static function generatePassword()
    {
        //password length is 8
        $password = [];
        $data = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','1','2','3','4','5','6','7','8','9','0'];
        $passwordIndex = array_rand($data,8);
        for($i=0;$i<8;$i++)
        {
            $password[$i] = $data[$passwordIndex[$i]];
        }
        return $password;

    }
    //scopes
    public function scopeEmployees($query)
    {
        return $query->where('role',User::EMPLOYEE);
    }


}
