<?php

namespace Database\Seeders;

use App\Models\Address;
use Illuminate\Database\Seeder;

class AddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Address::create([
            'user_id' => 1,
            'address' => 'shanti villa near guru teg bahadur colony',
            'landmark' => 'Jhulelal Trust School',
            'city' => 'ulhasnagar',
            'district' => 'thane',
            'state' => 'maharashtra',
            'postal_code' => '421002'
        ]);

        Address::create([
            'user_id' => 2,
            'address' => 'flat no 303 barkha apt near guru teg bahadur colony',
            'landmark' => 'Jhulelal Trust School',
            'city' => 'ulhasnagar',
            'district' => 'thane',
            'state' => 'maharashtra',
            'postal_code' => '421003'
        ]);

        Address::create([
            'user_id' => 3,
            'address' => 'flat no 203 barkha apt near guru teg bahadur colony',
            'landmark' => 'Jhulelal Trust School',
            'city' => 'ulhasnagar',
            'district' => 'thane',
            'state' => 'maharashtra',
            'postal_code' => '421003'
        ]);
    }
}
