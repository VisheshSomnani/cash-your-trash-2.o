<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Lavesh',
            'email' => 'lavesh@gmail.com',
            'password' => Hash::make('password'),
            'role' => 'employee'
        ]);
        User::create([
            'name' => 'Kamlesh',
            'email' => 'kamlesh@gmail.com',
            'password' => Hash::make('password')
        ]);
        User::create([
            'name' => 'Rajni',
            'email' => 'rajni@gmail.com',
            'password' => Hash::make('password')
        ]);
    }
}
