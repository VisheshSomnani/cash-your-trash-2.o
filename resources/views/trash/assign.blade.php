@extends('layouts.admin-panel.app')
@section('content')
<div class="content-wrapper">
    {{-- modal for assigning pickup --}}
    <div class="modal fade" id="assignModal">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Assign Pickup</h4>
            </div>
            <div class="modal-body">
                <form action="" id="assignForm" method="POST">
                    @csrf
                    @method('PUT')
                    <label for="assign">Assign Pickup to:</label>
                    <select  id="assign" value="Assign pickup to" name="assign">
                      @foreach ($employees as $employee)
                          <option value="{{$employee->id}}">{{$employee->loginDetails->name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="modal-footer justify-content-between">

                    {{-- <a href="{{route('trashes.assign')}}" class="btn btn-primary">Assign</a> --}}
                    <button type="submit" class="btn btn-primary">
                        Assign
                    </button>
                  </div>
                </form>

    </div>
        </div>
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Trash</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Trash</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Details</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>Collecting From</th>
                    <th>Type</th>
                    <th>Weight</th>
                    <th>Status</th>
                    <th>Collecting Person</th>
                    @if (auth()->user()->isAdmin())
                        <th>Actions</th>
                    @endif
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach ($trashes as $trash)
                        <tr>

                            <td>{{$trash->collecting_address()}}</td>
                            <td>{{$trash->type}}</td>
                            <td>{{$trash->getWeightInfo()}}</td>
                            <td>{{$trash->status}}</td>
                            <td> {{$trash->getCollectingPersonInfo()}}</td>
                            @if (auth()->user()->isAdmin())
                                <td> <button type="button" class="btn btn-success" onclick="assignForm({{$trash->id}})" data-toggle="modal" data-target="#assignModal" >
                                    Assign
                                  </button></td>
                                {{-- <td> {{$trash->getCollectingPersonInfo()}}</td> --}}
                            @endif

                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
@section('page-level-scripts')
<script>
    function assignForm(trashId)
    {
        var url = "/trashes/assign/"+trashId;
        $("#assignForm").attr('action',url);
    }
</script>

@endsection
