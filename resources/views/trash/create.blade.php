@extends('layouts.admin-panel.app')
@section('page-level-styles')
<link rel="stylesheet" href="../../plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<style>
    .btn-default
    {
        display: none;
    }
</style>
@endsection
@section('content')
@if(count($addresses)<1)

<button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default" data-backdrop="static">
    Launch Default Modal
  </button>
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Add Address</h4>
        </div>
        <div class="modal-body">
          <p>Seems Like You Didnt Added Your Address Yet</p>

        </div>
        <div class="modal-footer justify-content-between">

          <a href="{{route('addresses.create')}}" class="btn btn-primary">Add Address</a>
        </div>
</div>
@else
@extends('layouts.admin-panel.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Trash Pickup Booking</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Booking</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create Booking</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="{{route('trashes.store')}}" method="POST" id="quickForm">
                @csrf
                @method('POST')
                <div class="card-body">
                    <div class="form-group">
                        <label for="post">Address</label>
                        <select class="form-control select2" name="address_id" style="width: 100%;">
                            @foreach ($addresses as $address )
                                <option value="{{$address->id}}">{{$address->address}}</option>
                            @endforeach
                          </select>
                    </div>

                    <div class="form-group">
                        <label for="post">Trash Type</label>
                        <select class="form-control select2" name="type" style="width: 100%;">
                            <option value="dry">Dry</option>
                            <option value="wet">wet</option>
                        </select>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@endif
@endsection
@section('page-level-scripts')
<script>
    var element = document.getElementsByClassName("btn-default");
    if(element != null)
    {
        element[0].click();
    }
</script>
@endsection
